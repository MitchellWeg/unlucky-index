-- CREATE TABLE shots AS SELECT * FROM 'shooting/*.csv';

-- COPY shots TO 'shooting.parquet' (FORMAT PARQUET);

CREATE TABLE match_results AS SELECT * FROM 'match_results.csv';

COPY match_results TO 'match_results.parquet' (FORMAT PARQUET);

