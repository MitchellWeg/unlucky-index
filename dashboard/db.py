import duckdb


def db_init():
    duckdb.sql("DROP TABLE IF EXISTS match_results")
    duckdb.sql("DROP TABLE IF EXISTS shots")
    duckdb.sql("CREATE TABLE match_results AS SELECT * FROM '../data/fetcher/match_results.parquet'")
    duckdb.sql("CREATE TABLE shots AS SELECT * FROM '../data/fetcher/shooting.parquet'")
    duckdb.sql("UPDATE match_results SET Home_xG = 0.0 WHERE Home_xG = 'NA'")
    duckdb.sql("UPDATE match_results SET Away_xG = 0.0 WHERE Away_xG = 'NA'")

    return duckdb
