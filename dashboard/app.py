from db import db_init

from shiny.express import input, render, ui
from shinywidgets import render_plotly
import matplotlib.pyplot as plt

db = db_init()

ui.page_opts(title="Unlucky Index", fillable=True)


all_matches_df = db.sql('SELECT CAST(Date AS VARCHAR) AS Date, Day, Home, Away, HomeGoals, Home_xG, AwayGoals, Away_xG, Referee, MatchURL FROM match_results').df()

@render.ui
def rows():
    rows = input.all_matches_selected_rows()  
    link = ""
    if rows:
        link = all_matches_df.iloc[list(input.all_matches_selected_rows())]["MatchURL"].values[0]
    return f"{link}"

@render.ui
def search():
    inp = input.search_player()
    print(inp)

with ui.sidebar():
    ui.input_text(label="Search by player", id="search_player")
    ui.input_text(label="Search by team", id="search_team")

with ui.layout_columns(fill=False):
    with ui.value_box():
        "Average Home xG"
        db.sql("SELECT AVG(CAST(Home_xG AS decimal)) FROM match_results").fetchone()

    with ui.value_box():
        "Average Away xG"
        db.sql("SELECT AVG(CAST(Away_xG AS decimal)) FROM match_results").fetchone()


with ui.layout_columns(fill=False):
    with ui.card(full_screen=True):
        ui.card_header("All Matches")
        @render.data_frame
        def all_matches():
            return render.DataTable(all_matches_df, summary=False, row_selection_mode='single', filters=True)

    with ui.card(full_screen=True):
        ui.card_header("Top Teams by xG")
        @render.plot
        def grouped_by_xg():
            q = """SELECT Team, ROUND(SUM(ExpectedGoals),5) AS "Total xG"
                    FROM (
                        SELECT Home AS Team, CAST(Home_xG AS FLOAT) AS ExpectedGoals
                        FROM match_results
                        UNION ALL
                        SELECT Away AS Team, CAST(Away_xG AS FLOAT) AS ExpectedGoals
                        FROM match_results
                    ) AS CombinedData
                    GROUP BY Team ORDER BY "Total xG" DESC; """
            df = db.sql(q).df()

            df = df.sort_values(by=['Total xG'])

            return df.plot.bar(x='Team', y='Total xG')
            